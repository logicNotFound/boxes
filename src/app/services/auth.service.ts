import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { finalize, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // private user$ = new BehaviorSubject<User>(null);
  public token = new BehaviorSubject<string>("");
  protected authorized = new BehaviorSubject<boolean>(false);
  // protected apiUrl = environment.apiUrl;

  constructor(
    protected http: HttpClient,
    protected cookieService: CookieService
  ) {
    const saved_token = localStorage.getItem('token');
    if (saved_token) {
      this.token.next(saved_token);
    }
    this.token.subscribe(token => localStorage.setItem('token', token));
  }

  login(): Observable<any> {

    // console.log(localStorage.getItem('token'));

    /* Set Cookie */
    this.cookieService.set('Session', 's%3AxcJ_M2drwTMQryokUSN4WtEQVQu3k7hx.%2Bo3ahOS7Jp1ZjJTSW25w7yPCbX7qFot6%2BHCt%2BLYxk4M', undefined, undefined, 'api-staging.csgoroll.com');
    /* For some reason authentication didn't work */

    if (!localStorage.getItem('token')) {
      /* Redirect to steam for authentication */
      localStorage.setItem('token', "1");
      window.location.href = 'https://api-staging.csgoroll.com/auth/steam?redirectUri=http://localhost:4200';
    }
    return of(null);

  }

  logout(): any {
    // return this.http.post<void>(this.apiUrl + '/api/auth/logout', {})
    //   .pipe(finalize(() => {
    //     this.token.next(null);
    //     this.user$.next(null);
    //   }));
  }

  // registerUser(user: User): Observable<void> {
  //   return this.http.post<void>(this.apiUrl + '/api/auth/register/user', user);
  // }

  // requestResetPassword(userEmail: string): Observable<void> {
  //   return this.http.post<void>(this.apiUrl + '/api/auth/reset', { email: userEmail });
  // }

  // resetPassword(data: { password: string, password_confirmation: string }, urlCode: string): Observable<void> {
  //   return this.http.post<void>(this.apiUrl + '/api/auth/reset/' + urlCode, data);
  // }

  // status(): Observable<boolean> {
  //   // return this.token.pipe(map(token => token != null && token != 'null'));
  //   // return this.authorized;
  // }
}
