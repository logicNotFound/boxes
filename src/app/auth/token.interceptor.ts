import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { finalize, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(protected router: Router) { }


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const token = localStorage.getItem('token');


    request = request.clone({
      setHeaders: { Authorization: `Bearer ${token}` }
    });


    let obs: Observable<any>;
    obs = next.handle(request);

    // Let login attemps handle the error themselves
    if (request.url.includes('/auth/')) {
      return obs;
    }


    return obs.pipe(catchError(e => {
      if (e.status === 401 || e.status === 403) {
        return this.router.navigate(['/login']);
      }

      return throwError(e);
    }));
  }
}
