import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxviewComponent } from './boxview.component';

describe('BoxviewComponent', () => {
  let component: BoxviewComponent;
  let fixture: ComponentFixture<BoxviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
