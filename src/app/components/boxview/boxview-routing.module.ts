import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../guards/auth-guard.service';
import { BoxviewComponent } from './boxview.component';


const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthGuard], // Uncomment to initialize login
    component: BoxviewComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoxViewRoutingModule { }
