import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxViewRoutingModule } from './boxview-routing.module';
import { BoxviewComponent } from './boxview.component';


@NgModule({
  declarations: [
    BoxviewComponent,
  ],
  imports: [
    CommonModule,
    BoxViewRoutingModule,
  ],
  exports: [],
  providers: []
})
export class BoxViewModule { }
