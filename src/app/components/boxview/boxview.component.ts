import { Component, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { Box } from '../../models/Box';
import { Boxes } from '../../models/Boxes';

@Component({
  selector: 'app-boxview',
  templateUrl: './boxview.component.html',
  styleUrls: ['./boxview.component.scss']
})
export class BoxviewComponent implements OnInit {

  public boxes: Boxes | undefined;
  public loading = true;
  public error: any;
  public openedBox: any;

  constructor(private apollo: Apollo,) { }

  ngOnInit(): void {
    this.apollo
      .watchQuery({
        query: gql`
        query Boxes{
          boxes(free: false, purchasable: true, openable: true)
          {
            total
            edges {
              node {
                id
                order
                openable
                name,
                description,
                iconUrl,
                price
              }
            }
          }
        }
      `,
      })
      .valueChanges.subscribe((result: any) => {
        console.log('result ---> ', result);

        this.boxes = result?.data?.boxes;
        this.loading = result.loading;
        this.error = result.error;

        console.log('Boxes', this.boxes);
      });
  }

  openBox(boxId: string | undefined) {

    this.apollo.mutate({
      mutation: OPEN_BOX,
      variables: {
        input: {
          boxId: boxId,
          amount: 1
        }
      }
    }).subscribe(({ data }) => {
      console.log('got data', data);
      this.openedBox = data;
    }, (error) => {
      console.log('There was an error sending the query', error);
    });
  }
}

/* GraphQL */
const OPEN_BOX = gql`
mutation BoxOpen($input: OpenBoxInput!) {
  openBox(input: $input) {
    boxOpenings {
      id
      boxId
  }
}
}
`;
