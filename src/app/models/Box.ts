// Class for type safety.
export class Box {

  id?: string;
  order?: number;
  openable?: boolean;
  name?: string;
  description?: string;
  iconUrl?: string;
  price?: number;

  constructor(
    // data: any = {}
    data?: {
      id?: string,
      order?: number,
      openable?: boolean,
      name?: string,
      description?: string,
      iconUrl?: string,
      price?: number;
    }
  ) {
    this.id = data?.id;
    this.order = data?.order;
    this.openable = data?.openable;
    this.name = data?.name;
    this.description = data?.description;
    this.iconUrl = data?.iconUrl;
    this.price = data?.price;
  }
}
