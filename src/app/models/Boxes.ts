import { Box } from "./Box";

// Class for type safety.
export class Boxes {
  total?: number;
  edges?: { node: Box }[];

  constructor(
    // data: any = {}
    data?: {
      total?: number,
      edges?: { node: Box }[]
    }
  ) {
    this.total = data?.total;
    this.edges = data?.edges;
  }
}
