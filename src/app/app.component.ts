import { AuthService } from './services/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private authService: AuthService) {

  }

  ngOnInit(): void {

    this.authService.login().subscribe(resp => {
      console.log('What we got back --> ', resp);
    });
  }
}
